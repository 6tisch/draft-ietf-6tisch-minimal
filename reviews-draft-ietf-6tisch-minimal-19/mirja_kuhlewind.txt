https://www.ietf.org/mail-archive/web/6tisch/current/msg05184.html

===============================================================================
===============================================================================
===============================================================================

Mirja K�hlewind has entered the following ballot position for
draft-ietf-6tisch-minimal-19: No Objection

When responding, please keep the subject line intact and reply to all
email addresses included in the To and CC lines. (Feel free to cut this
introductory paragraph, however.)


Please refer to https://www.ietf.org/iesg/statement/discuss-criteria.html
for more information about IESG DISCUSS and COMMENT positions.


The document, along with other ballot positions, can be found here:
https://datatracker.ietf.org/doc/draft-ietf-6tisch-minimal/



----------------------------------------------------------------------
COMMENT:
----------------------------------------------------------------------

A couple of comments mostly on the use of normative language:

1) The following sentence contradict a little all other normative
language in this document (basically making everything a SHOULD):
"Any 6TiSCH compliant device SHOULD implement this mode of operation."
Why is this a upper case SHOULD? Is this sentence needed at all? 

--------------
ANSWER 1

The SHOULD will be lower cased as per other reviews received as well. 
--------------

2) Here the usage of normative language is also not clear to me:
"The default values of the TSCH Timeslot template (defined in
   [IEEE802154-2015]  section 8.4.2.2.3) and Channel Hopping sequence
   (defined in [IEEE802154-2015] section 6.2.10) SHOULD be used.  A
node
   MAY use different values by properly announcing them in its Enhanced
   Beacon."

--------------
ANSWER 2

Well, we recommend the use of default values with the SHOULD. If someone wants to use different values then those need to be announced in the EB. 
--------------

3) Is it correct that these are SHOULDs?
"EB Destination Address field SHOULD be set to 0xFFFF (short broadcast
   address).  The EB Source Address field SHOULD be set as the node's
   short address if this is supported.  Otherwise the long address
   SHOULD be used."

--------------
ANSWER 3

As per another reviewer comment the second SHOULD has been turned into a MUST. 
--------------

4) What the recommended value for EB_PERIOD?:
"In a minimal TSCH
   configuration, a node SHOULD send an EB every EB_PERIOD."

--------------
ANSWER 4

We discussed this in the ML and during WG calls multiple times. We cannot state a value as this depends on the particular network, slotframe size, energy consumption requirements, limitations in network formation time, etc...  We say here that there is a period for sending EBs.
--------------

5) This is also a weird SHOULD for me:
"EBs SHOULD be used to obtain information about local networks, ..."
What else should be used? Or you just don't obtain the information you
need. I actually guess that you should simply use a lower case should in
this sentence.
Should it be?:
"The default values of the TSCH Timeslot template (defined in
   [IEEE802154-2015]  section 8.4.2.2.3) and Channel Hopping sequence
   (defined in [IEEE802154-2015] section 6.2.10) SHOULD be used.  If a
node
   uses different values, it MUST properly announcing them in its
Enhanced
   Beacon."
Or is this sentence just duplicating the SHOULD of the previous sentence
slightly differently?

--------------
ANSWER 5

The EB is used to announce the network, distribute the initial configuration and provide a synchronization mechanism until the nodes have not been authenticated. The first SHOULD has been turned into lower cased as suggested. 
--------------

6) Maybe also name the recommended values for MAX_EB_DELAY and
NUM_NEIGHBOURS_TO_WAIT in section 6.2 or give a forward reference to
section 7.3?

--------------
ANSWER 6

Thanks this has been done. 
--------------

7) "At any time, a node MUST maintain connectivity to at least one time
   source neighbor. "
  How can you ensure that you can maintain connectivity? I guess this
must be a SHOULD... or what do you mean exactly by 'maintain
connectivity'?

--------------
ANSWER 7

We propose to change connectivity by synchronization. Losing synchronization means losing the possibility to communicate with the network and forces a node to join again. This will also be stated. 
--------------

8) I think this sentence is confusing given the recommended value for
NUM_UPPERLAYER_PACKETS is 1:
"One entry in the queue is reserved at all times for frames of type
BEACON."
I guess what you want to say it that if a BEACON should be send and there
is no space in the queue one frame should be drop and the BEACON should
be queued instead?

--------------
ANSWER 8

Thanks, this has been corrected as suggested by the reviewer.
--------------

9) I'm a little surprised to not see anything about 6LoWPAN in the body
of the document (besides the intro in section 1). Shouldn't there also be
some normative language on the use of 6LoWPAN?

--------------
ANSWER 9

Minimal provides a configuration to bootstrap the network. 6LoWPAN framework is used unmodified.
--------------

One editorial comment (from the abstract):
I don't really understand this sentence
"A minimal mode of operation is a baseline set of protocols, ..."
I guess you want to say something like
"This minimal mode of operation specifies the baseline set of protocols
that need to be supported, .."

--------------
ANSWER 10

Thanks, this has been also modified according to the comment.
--------------
