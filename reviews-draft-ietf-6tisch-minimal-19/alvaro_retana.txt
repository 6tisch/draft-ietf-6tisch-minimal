Alvaro Retana's Discuss on draft-ietf-6tisch-minimal-19: (with DISCUSS and COMMENT) 

Sent to The IESG <iesg@ietf.org>
cc: draft-ietf-6tisch-minimal@ietf.org,
6tisch-chairs@ietf.org,
pthubert@cisco.com

===============================================================================
===============================================================================
===============================================================================

----------------------------------------------------------------------
DISCUSS:
----------------------------------------------------------------------

I agree with Mirja's comments about the use of Normative Language and
believe that for a BCP document its use is not clear enough such that it
could result in different "compliant" implementations that may not
interoperate properly.  So I am making the issue a DISCUSS.

As Mirja mentioned, this statement in the Introduction opens the door for
other configurations: "Any 6TiSCH compliant device SHOULD implement this
mode of operation."  The question to answer for this "SHOULD" (and
others) is what are the particular circumstances when ignoring this
recommendation is ok.  The document doesn't elaborate at all on
details.

--------------
Answer 1

As per Mirja comment this has been changed to a lower case should. 

--------------

I understand that there are in fact other posible configurations, but if
this document is describing the Best Current Practice then I think it
should be explicit as to what that BCP is.

--------------
Answer 2

This document defines a profile or operation mode that uses particular parameters of IEEE802.15.4-2015. 
The document also specifies how some of those parameters can be used with non-default values, in order to be flexible and support different networks or applications without losing interoperability.
Yet, the draft specifies those that are strict in their use (e.g. 1 active slot with default timeslot = 0x0000 and default channel.offset = 0x0000 or the use of the default channel hopping sequence), this are the default values that we recommend or mandate, but this does not preclude that other parameters are equally good (e.g. timeslot =0x0001 and default channel.offset = 0x0001 or a different ch.hopping sequence) and hence we indicate that if someone wants to change them, this is possible but has to use the specific IEEE802.15.4 mechanism to do so (e.g announce them in the properly in the EB.)
--------------

Another example of the lack of clarity (besides those already mentioned
by Mirja) is the use of RPL:  The Introduction mentions that "RPL is
specified to provide the framework for time synchronization in an
802.15.4 TSCH network.", but Section 5 (RPL Settings) makes it optional:
"In a multi-hop topology, the RPL routing protocol [RFC6550] MAY be
used."

--------------
Answer 3

A minimal network can use other routing protocols, such as mesh under routing. The used routing protocol does not impact to the MAC layer configurations defined by the specification. In opposition, we provide the parameters in case RPL is used as we consider it a good match. Just to compare, the RFC6282 defines header compression mechanisms up to UDP but a 6LoWPAN network using IPHC may not use UDP at all. Here we do the same, we think RPL may be used frequently and hence this specification. The key match here is the mapping of the routing topology and the l2 topology through the join metric if RPL is used.
--------------

----------------------------------------------------------------------
COMMENT:
----------------------------------------------------------------------

s/I-D.ietf-6lo-routing-dispatch/I-D.ietf-roll-routing-dispatch

--------------
Answer 4

Good catch. Thanks! 
--------------
